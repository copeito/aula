<?php
namespace App\Classes\copeito\form\item;

use App\Classes\copeito\lazyLoader\LazyLoader;

use Illuminate\View\View;

abstract class Item
{
    protected string $name;
    protected string $htmlAttribute;

    protected $attributes = array();

    abstract public function render(): view;

    use LazyLoader {
        init as lazyLoaderInitializer;
    }

    public function __construct(string $name)
    {
        $this->name = $name;

        $this->defineLazyLoading([
            'attributes' => function() {
                return [
                    'name' => $this->name,
                    'id' => $this->name,
                ];
            },
            'htmlAttribute' => function() {
                return $this->getHtmlAttribute();
            }
        ]);
    }

    protected function defineLazyLoading(array $definitions)
    {
        $this->lazyLoaderInitializer(
            $definitions
        );
    }

    protected function getHtmlAttribute(): string
    {
        $attributes = '';

        foreach((array)$this->attributes as $attribute => $value) {
            $attributes .= ($attributes ? ' ' : '').$attribute;

            if (isset($value)) {
                $attributes .= '="'.$value.'"';
            }
        }

        return $attributes;
    }

    public function __get(string $prop) : mixed
    {
        if (in_array($prop, array('name'))) {
            return $this->{$prop};
        } else {
            return $this->getValue($prop);
        }
    }

    public function set(string $field, string $value = null) : static
    {
        /*
            Workaround para evitar el error lanzado por php al utilizar 
            $this->attributes[$field] = $value;
        */
        $attr = $this->attributes;

        $attr[$field] = $value;

        $this->attributes = $attr;

        return $this;
    }
}