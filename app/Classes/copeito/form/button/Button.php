<?php
namespace App\Classes\copeito\form\button;

use Illuminate\Support\Facades\View;
use App\Classes\copeito\form\item\Item;
use Illuminate\View\View as ViewResponse;

class Button extends Item
{
    protected string $type;
    protected string $label;

    public function __construct(string $name = '', string $type = 'button')
    {
        $this->type = $type;

        parent::__construct($name);
    }

    protected function defineLazyLoading(array $definitions)
    {
        parent::defineLazyLoading(
            array_merge_recursive([
                'label' => function () {
                    return $this->name;
                }
            ]),
            $definitions
        );
    }

    public function setLabel(string $label)
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel() : string
    {
        return $this->label;
    }

    public function render(): ViewResponse
    {
        View::addLocation(__DIR__.'/views/');

        return view(
            'button', [
                'attributes' => $this->getHtmlAttribute(),
                'label' => $this->label
            ]
        );

    }
}
