<?php
namespace App\Classes\copeito\form\input;

class Label {
    protected string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }
}