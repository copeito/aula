<?php
/**
 * Provides tools to create and manage forms
 * 
 * @author David Rey <copeito@gmail.com>
 */
namespace App\Classes\copeito\form;

use App\Classes\copeito\form\input\Input;
use App\Classes\copeito\form\button\Button;
use App\Classes\copeito\lazyLoader\LazyLoader;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Response as FacadesResponse;
use Illuminate\Support\Facades\View;

class Form
{
    protected $items = array();
    protected $labels = array();
    protected $buttons = array();

    protected $attributes = array();

    protected string $method;

    use LazyLoader {
        init as lazyLoaderInitializer;
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->defineLazyLoading([
            'attributes' => function(){
                return [
                    'method' => 'POST'
                ];
            }
        ]);
    }

    /**
     * Specifies method to use
     *
     * @param string $method method
     *
     * @return Form
     */
    public function setMethod(string $method) : Form
    {
        $this->method = strtoupper($method);

        return $this;
    }

    /**
     * Set attributes
     *
     * @param string $field field
     * @param string $value value
     *
     * @return Form
     */
    public function set(string $field, string $value) : Form
    {
        /*
            Workaround para evitar el error lanzado por php al utilizar 
            $this->attributes[$field] = $value;
        */
        $attr = $this->attributes;

        $attr[$field] = $value;

        $this->attributes = $attr;

        return $this;
    }

    /**
     * Initializes lazy loader
     *
     * @param array $definitions definitions
     *
     * @return void
     */
    protected function defineLazyLoading(array $definitions): void
    {
        $this->lazyLoaderInitializer(
            $definitions
        );
    }

    /**
     * Return attritubes html code
     *
     * @return string
     */
    protected function getHtmlAttribute(): string
    {
        $attributes = '';

        foreach((array)$this->attributes as $attribute => $value) {
            $attributes .= ($attributes ? ' ' : '').$attribute;
            if (isset($value)) {
                $attributes .= '="'.$value.'"';
            }
        }

        return $attributes;
    }

    /**
     * Retrieves form items
     *
     * @return array
     */
    public function getItems() : array
    {
        return $this->items;
    }

    /**
     * Add input to form
     *
     * @param Input $input input
     * @param string|null $label label
     *
     * @return Form
     */
    public function addInput(Input $input, string $label = null) : void
    {
        $name = $input->name;

        if (@$this->items[$name]){
            throw new \Exception(
                'Ya existe un item con el nombre '.$name." asociado al formulario"
            );
        }

        $this->labels[$name] = $label;
        $this->items[$name] = $input;
    }

    /**
     * Add button to form
     *
     * @param Button $button Button
     *
     * @return Form
     */
    public function addButton(Button $button): Form
    {
        $this->buttons[] = $button;

        return $this;
    }

    /**
     * Renderizes form
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render(): \Illuminate\Contracts\View\View
    {
        View::addLocation(__DIR__ .'/views/');

        $items = $this->items;
        $labels = $this->labels;
        $buttons = $this->buttons;
        $attributes = $this->attributes;
        $method = ($this->method ?? null);

        return view(
            'form', 
            compact(
                'items', 'labels', 'buttons', 'method'
            ),[
                'attributes' => $this->getHtmlAttribute()
            ]
        );
    }
}