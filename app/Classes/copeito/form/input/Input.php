<?php
namespace App\Classes\copeito\form\input;

use App\Classes\copeito\db\field\Field;

use Illuminate\Support\Facades\View;
use Illuminate\View\View as ViewResponse;

use App\Classes\copeito\form\item\Item;

class Input extends Item
{
    protected string $type;

    protected $attributes = array();

    public function __construct(string $name, string $type = null)
    {
        $this->type = $type;

        parent::__construct($name);
    }

    public static function instanceFromField(Field $field) : static
    {
        return new static(
            $field->name,
            'text'
        );
    }

    protected function defineLazyLoading(array $definitions)
    {
        parent::defineLazyLoading(
            array_merge_recursive([
                'attributes' => function () {
                    return [
                        'name' => $this->name,
                        'id' => $this->name,
                        'type' => $this->type
                    ];
                }
            ]),
            $definitions
        );
    }

    public function render() : ViewResponse
    {
        View::addLocation(__DIR__.'/views/');

        return view(
            'input', [
                'attributes' => $this->getHtmlAttribute(),
            ]
        );
    }
}