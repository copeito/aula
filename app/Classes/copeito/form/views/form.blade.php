<form {!!$attributes!!}>
    <div class="row">
        @csrf

        @if(isset($method))
            @method($method)
        @endif

        <div class="row">
            @foreach($items as $name => $item)
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>{{ $labels[$name] }}:</label>
                        {!! $item->render() !!}
                    </div>
                </div>
            @endforeach
            @foreach($buttons as $button)
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        {!! $button->render() !!}
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</form>