<?php
namespace App\Classes\copeito\db\field;

use \Illuminate\Support\Facades\DB;

use \App\Classes\copeito\db\table\Table;
use \App\Classes\copeito\lazyLoader\LazyLoader;
use phpDocumentor\Reflection\Types\Boolean;

class Field
{
    protected string $name;
    protected object $properties;
    protected Table $table;
    protected string $type;

    use LazyLoader {
        init as lazyLoaderInitializer;
        __get as lazyGet;
    }

    public function __construct(string $name, Table $table)
    {
        $this->name = $name;
        $this->table = $table;
        
        $this->defineLazyLoading();
    }

    public function __get(string $name) : mixed
    {
        $result = null;

        if ($name == 'name') {
            $result = $this->name;
        } else {
            $result = $this->lazyGet($name);
        }

        return $result;
    }

    protected function getProperties() : object
    {
        return DB::select(
            'show fields from '.$this->table.' like "'.$this->name.'"'
        )[0];
    }

    protected function defineLazyLoading()
    {
        $this->lazyLoaderInitializer(
            array(
                'properties' => function() {
                    return $this->getProperties();
                },
                'type' => function() {
                    return $this->properties->Type;
                },
                'allowsNull' => function() {
                    return ($this->properties->Null == 'YES');
                },
                'isPrimaryKey' => function() {
                    return ($this->properties->Key == 'PRI');
                },
            )
        );
    }

     public function __toString() : string
    {
        return $this->name;
    }
}