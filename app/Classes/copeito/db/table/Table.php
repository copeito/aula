<?php

namespace App\Classes\copeito\db\table;

use \Illuminate\Support\Facades\DB;
use \App\Classes\copeito\db\field\Field;

class Table
{
    protected string $name;

    public function __construct(string $name)
    {
        $this->name = $name;    
    }

    public function getFields() : array
    {
        $fields = array();

        $fieldNames = DB::getSchemaBuilder()->getColumnListing($this->name);

        foreach ($fieldNames as $fieldName) {
            $fields[$fieldName] = new Field($fieldName, $this);
        }

        return $fields;
    }

    public function getPrimaryKey() : Field|null
    {
        foreach ($this->getFields() as $field) {
            if ($field->isPrimaryKey) {
                return $field;        
            }
        }        
    }

    public function __toString() : string
    {
        return $this->name;
    }
}