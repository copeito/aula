<table {!! $attributes !!}>
    <thead>
        @foreach ($labels['header'] as $label)
            <th>{{ $label }}</th>
        @endforeach
    </thead>
    <tfoot>
        @foreach ($labels['footer'] as $label)
            <th>{{ $label }}</th>
        @endforeach
    </tfoot>
</table>
