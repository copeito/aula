<?php
namespace App\Classes\copeito\table;

use Illuminate\Support\Facades\View;

class Table
{
    protected $attributes = array();
    protected $labels = array();

    public function __construct(string $id)
    {
        $this->addAttribute('id', $id);
    }

    public function addAttribute(string $name, string $value) : void
    {
        $this->attributes[$name] = $value;
    }

    public function addAttributes(array $attributes) : void
    {
        foreach ($attributes as $name => $value) {
            $this->addAttribute($name, $value);
        }
    }

    public function setHeaderLabels(array $labels)
    {
        $this->setLabels($labels, 'header');
    }

    public function setFooterLabels(array $labels)
    {
        $this->setLabels($labels, 'footer');
    }

    public function setLabels(array $labels, $positions = array('header', 'footer'))
    {
        foreach ((array)$positions as $position) {
            $this->labels[$position] = $labels;
        }
    }

    public function render()
    {
        View::addLocation(__DIR__ .'/views/');

        $attributes = '';

        foreach($this->attributes as $attribute => $value) {
            $attributes .= ($attributes ? ' ' : '').$attribute.'="'.$value.'"';
        }

        return view(
            'table', [
                'attributes' => $attributes,
                'labels' => $this->labels,
            ]
        );
    }
}