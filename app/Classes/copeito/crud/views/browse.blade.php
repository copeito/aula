@extends('layout')

@section('css')
<link rel="stylesheet" href="{{ asset('css/crud/browse.css') }}">
<script src="{{ asset('js/crud/browse.js') }}"></script>
@stop

@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ $createRoute }}">Nuevo</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
    @endif

    {!! $dataTable->render() !!}
@endsection