<?php
/**
 * CRUD Controller base class extendable to create CRUD controllers
 * 
 * @author David Rey <copeito@gmail.com>
 */
namespace App\Classes\copeito\crud;

use App\Classes\copeito\model\Model;

use App\Classes\copeito\form\Form;
use App\Classes\copeito\form\input\Input;
use App\Classes\copeito\form\button\Button;

use App\Classes\copeito\lazyLoader\LazyLoader;

use App\Classes\copeito\db\table\Table;
use App\Classes\copeito\db\field\Field;
use App\Classes\copeito\dataTable\DataTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class Crud extends \App\Classes\copeito\controller\Controller
{
    use LazyLoader {
        init as lazyLoaderInitializer;
    }

    protected string $model;

    protected $insertableFields = array();
    protected $editableFields = array();
    protected $writableFields = array();
    protected $browsableFields = array();
    protected $fields = array();

    protected $browseLabels = array();

    protected Table $table;

    protected Form $form;

    protected static $browsables = [];

    protected string $uriDataService;
    protected string $createRoute;
    protected string $browseRoute;

    /**
     * Class constructor
     * 
     */
    public function __construct()
    {
        if (!isset($this->model)) {
            throw new \Exception(
                'La propiedad Crud::model no ha sido definida'
            );
        } elseif (!is_subclass_of($this->model, Model::class)) {
            throw new \Exception(
                'La propiedad Crud::model no apunta a una clase derivada de '.Model::class
            );
        }

        View::addLocation(__DIR__ .'/views/');

        $this->defineLazyLoading();
    }

    /**
     * Retrieves fields to use in creating record
     *
     * @return array
     */
    protected function getInsertableFields() : array
    {
        return $this->writableFields;
    }

    /**
     * Retrieves fields to use editing record
     *
     * @return array
     */
    protected function getEditableFields() : array
    {
        return $this->writableFields;
    }

    /**
     * Fields to show in browse mode
     *
     * @return array
     */
    protected function getBrowsableFields() : array
    {
        $fields = [];

        if (isset(static::$browsables)) {
            foreach(static::$browsables as $field) {
                $fields[$field] = $this->fields[$field];
            }
        }

        return $fields;
    }

    /**
     * Retrieves writable fields
     *
     * @return array
     */
    protected function getWritableFields() : array
    {
        $writables = array();

        $fillables = (new $this->model)->getFillable();

        foreach ($fillables as $fillable) {
            $writables[$fillable] = $this->fields[$fillable];
        }

        return $writables;
    }

    /**
     * Retrieves table used in model
     *
     * @return Table
     */
    protected function getTable() : Table
    {
        return new Table(
            (new $this->model)->getTable()
        );
    }

    /**
     * Retrieves all fields in model table
     *
     * @return array
     */
    protected function getFields() : array
    {
        return $this->table->getFields();
    }

    /**
     * Initializes lazy loader
     *
     * @return void
     */
    protected function defineLazyLoading() : void
    {
        $this->lazyLoaderInitializer([
                'insertableFields' => function() {
                    return $this->getInsertableFields();
                },
                'table' => function() {
                    return $this->getTable();
                },
                'fields' => function() {
                    return $this->getFields();
                },
                'writableFields' => function() {
                    return $this->getWritableFields();
                },
                'editableFields' => function() {
                    return $this->getEditableFields();
                },
                'browsableFields' => function() {
                    return $this->getBrowsableFields();
                },
                'browseLabels' => function() {
                    return $this->getBrowseLabels();
                },
                'uriDataService' => function() {
                    return Request()->segments()[0].'/getData/';
                },
                'form' => function () {
                    return $this->getForm();
                },
                'createRoute' => function () {
                    return route(Request()->segments()[0].'.create');
                },
                'browseRoute' => function () {
                    return route(Request()->segments()[0].'.index');
                } 
        ]);
    }

    /**
     * Retrieves label to show for given field
     *
     * @param Field $field Field
     *
     * @return string
     */
    protected function getLabel(Field $field) : string
    {
        return $field->name;
    }

    /**
     * Retrieved labels to use in browse mode
     *
     * @return array
     */
    protected function getBrowseLabels() : array
    {
        $labels = array();

        foreach ($this->browsableFields as $field) {
            $labels[] = $this->getLabel($field);
        }

        return $labels;
    }

    /**
     * Create submit button
     *
     * @return Button
     */
    protected function getSubmitButton() : Button
    {
        return new Button('submit', 'submit');
    }

    /**
     * Creates input for given field
     * 
     * @param Field $field Field
     * 
     * @return Input
     */
    protected function getInput(Field $field) : Input
    {
        $input = Input::instanceFromField($field);
        
        if (!$field->allowsNull) {
            $input->set('required');
        }

        return $input;
    }

    /**
     * Get value to display for given field
     *
     * @param Field $field Field
     * @param array $row row
     *
     * @return string
     */
    protected function getDisplayableValue(Field $field, array $row) : string
    {
        $primaryKey = $this->table->getPrimaryKey();

        if ((string)$field == (string)$primaryKey) {
            $form = (new form())->set(
                'action', 
                route(Request()->segments()[0].'.destroy', $row[$primaryKey->name])
            );

            $button = (new Button('', 'submit'))
                ->set('class', 'btn btn-danger')
                ->setLabel('borrar');

            $form->addButton($button);

            $form->setMethod('DELETE');

            $value = (string)$form->render();
        } else {
            $value = '<a href="'.route(
                Request()->segments()[0].'.edit', 
                $row[$primaryKey->name]
            ).'">'.$row[$field->name].'</a>';
        }

        return $value;
    }

    /**
     * Deletes given record
     *
     * @param mixed $id id
     *
     * @return void
     */
    public function destroy(Model $model)
    {
        $model->delete();

        return redirect()->route(Request()->segments()[0].'.index')
            ->with('success','Record deleted successfully');
    }

    /**
     * Retrieves data for use in DataTables component
     *
     * @param Request $request request
     *
     * @return JsonResponse
     */
    public function getData(Request $request): JsonResponse
    {
        $draw = $request->input('draw');
        $order = $request->input('order');
        $columns = $request->input('columns');
        $start = $request->input('start');
        $length = $request->input('length');
        $search = $request->input('search');
        $format = $request->input('format') ?: 'html';
        $end = ($length + $start);  
        
        $orderFieldName = array_keys($this->browsableFields)[$order[0]['column']];

        $data = $this->model::orderBy(
            $orderFieldName,
            $order[0]['dir']
        )->get()->toArray();

        $values = [];

        $recordsTotal = count($data);

        $num = 0;

        foreach($data as $row){
            if (($num >= $start) && ($num < $end)){
                if ($num < $end) {
                    $values = [];

                    foreach($row as $fieldName => $value){
                        if ($format == 'html') {
                            $values[] = $this->getDisplayableValue($this->fields[$fieldName], $row);                            
                        } elseif ($format == 'json') {
                            $values[$fieldName] = $value;
                        }
                    }
        
                    $rows[] = $values;
                } else {
                    break;
                }
            }
            
            $num++;
        }

        $output = [
            'draw' => (int)$draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => $rows
        ];

        return response()->json($output);
    }

    public function show()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        $createRoute = $this->createRoute;

        $dataTable = new DataTable();

        $dataTable->setDataService(
            $this->uriDataService
        );

        $dataTable->setLabels(
            $this->browseLabels
        );

        $dataTable->addAttributes([
            'id' => 'browse',
        ]);

        return view(
            'browse',
            compact(
                'dataTable',
                'createRoute'
            )
        );
    }

    /**
     * Create form
     *
     * @return Form
     */
    protected function getForm(): Form
    {
        $form = new Form();

        foreach ($this->insertableFields as $field) {
            $form->addInput(
                $this->getInput($field),
                $this->getLabel($field)
            );
        }

        $form->addButton(
            $this->getSubmitButton()
        );

        return $form;
    }

    /**
     * Updates record
     *
     * @param Request $request request
     * @param mixed $id id
     *
     * @return void
     */
    public function update(Request $request, Model $model)
    {
        $validation = [];

        foreach ($this->insertableFields as $field) {
            $val = ($field->allowsNull ?: 'required');
            $validation[$field->name] = $val;
        }

        $request->validate($validation);

        $model->update($request->all());

        return redirect()->route(Request()->segments()[0].'.index')
        ->with('success','Record updated successfully.');
    }

    /**
     * Inserts record
     *
     * @param Request $request request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $validation = [];

        foreach ($this->insertableFields as $field) {
            $val = ($field->allowsNull ?: 'required');
            $validation[$field->name] = $val;
        }

        $res = $request->validate($validation);

        $this->model::create($request->all());

        return redirect()->route(Request()->segments()[0].'.index')
            ->with('success','Record created successfully.');
    }

    /**
     * Shows edit form
     *
     * @param mixed $id id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Model $model): \Illuminate\Contracts\View\View
    {
        $browseRoute = $this->browseRoute;

        $form = $this->form;

        $form->setMethod('PUT');

        $form->set(
            'action', 
            route(Request()->segments()[0].'.update', $model)
        );

        foreach($form->getItems() as $item) {
            $item->set('value', $model->{$item->name});
        }

        return view('edit',compact('model', 'form', 'browseRoute'));
    }

    /**
     * Shows create form
     * 
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): \Illuminate\Contracts\View\View
    {
        $form = $this->form;

        $form->set(
            'action', 
            route(Request()->segments()[0].'.store')
        );

        $browseRoute = $this->browseRoute;

        return view(
            'create',
            compact(
                'form',
                'browseRoute'
            )
        );        
    }

}