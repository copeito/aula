<div class="row" style="margin-top: 5rem;">
    {!! $table !!}
</div>
<script>
    $(document).ready(function(){
        $({{ $id }}).dataTable(
            {!! $config !!}
        );
    });
</script>