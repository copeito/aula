<?php
namespace App\Classes\copeito\dataTable;

use Illuminate\Support\Facades\View;
use App\Classes\copeito\table\Table;

use App\Classes\copeito\lazyLoader\LazyLoader;

class DataTable extends Table
{
    protected string $dataService;

    use LazyLoader {
        init as lazyLoaderInitializer;
    }

    public function __construct(string $id = 'browse')
    {
        parent::__construct($id);

        $this->defineLazyLoading();
    }

    protected function defineLazyLoading()
    {
        $this->lazyLoaderInitializer([
            'config' => function() {
                return [
                    'processing' => true,
                    'serverSide' => true,
                    'ajax' => $this->dataService,
                ];
            }
        ]);
    }

    public function setDataService(string $uri)
    {
        $this->dataService = $uri;
    }

    public function addConfig(string $field, mixed $value)
    {
        $this->config = array_merge_recursive(
            $this->config,[
                $field => $value
            ]
        );
    }

    public function render()
    {
        View::addLocation(__DIR__ .'/views/');

        $table = parent::render();
        $id = $this->attributes['id'];

        $config = json_encode(
            $this->config
        );

        return view(
            'dataTable',
            compact(
                'table', 'id', 'config'
            )
        );
    }
}