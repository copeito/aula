<?php
namespace App\Http\Controllers;

use App\Classes\copeito\form\input\Input;
use App\Classes\copeito\form\button\Button;

use App\Classes\copeito\db\field\Field;

class BaseCrudController extends \App\Classes\copeito\crud\Crud
{
    protected function getInput(Field $Field) : Input
    {
        return parent::getInput($Field)->
            set('class', 'form-control')->
            set('placeholder', $this->getLabel($Field));
    }

    protected function getSubmitButton() : Button
    {
        return parent::getSubmitButton()
            ->set('class', 'btn btn-primary')
            ->setLabel('Guardar');
    }
}