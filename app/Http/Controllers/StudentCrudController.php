<?php
namespace App\Http\Controllers;

use App\Classes\copeito\db\field\Field;

use App\Models\Student;

class StudentCrudController extends BaseCRUDController
{
    protected string $model = Student::class;
    protected static $browsables = ['id', 'name', 'surnames', 'email'];

    protected function getLabel(Field $Field) : string
    {
        $label = '';

        if ($Field->name == 'name') {
            $label = 'nombre';
        } elseif ($Field->name == 'surnames') {
            $label = 'apellidos';
        } else {
            $label = $Field->name;
        }

        return $label;
    }
}
