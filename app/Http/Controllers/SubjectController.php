<?php

namespace App\Http\Controllers;

use App\Models\Subject;

class SubjectController extends \App\Classes\copeito\crud\Crud
{
    protected string $model = Subject::class;
}
