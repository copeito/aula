<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use \App\Classes\copeito\model\Model;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
}
