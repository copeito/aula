<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Classes\copeito\model\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'surnames', 'email'];
}
