<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentCrudController;
use App\Http\Controllers\SubjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});

Route::resource('students', StudentController::class);

Route::get('studentCrud/getData/', [StudentCrudController::class, 'getData']);

Route::model('studentCrud', 'App\Models\Student');
Route::resource('studentCrud', StudentCrudController::class);

Route::resource('subjects', SubjectController::class);

Route::middleware(['cors'])->group(function () {
    return Route::get('studentCrud/getData/', [StudentCrudController::class, 'getData']);
});